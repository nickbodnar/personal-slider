var gui = require('nw.gui');
var fs = require('fs');
var path = require('path');
var os = require('os');
var spawn = require('child_process').spawn;

process.on('uncaughtException', error);

// BELOW KINDA "WORKS" FOR DF 4
// var net = require('net');

// var server = net.createServer(function(socket) {
//     console.log(socket)
//     socket.write('Echo server\r\n');
//     socket.on('data', function (data) {
//         console.log(data.toString('ascii'));
//         console.log(data);
//         // socket.write(data);
//         setTimeout(function () {
//             socket.write("S 1\r\n");
//         }, 10000)
//     }).on('end', function () {
//         // socket.destroy();
//     });
//     // socket.pipe(socket);
// });

// server.listen(80);

// gui.Window.get().showDevTools();

function resize() {
    gui.Window.get().height = $('body').innerHeight();
}

function isMac() {
    return process.platform == 'darwin';
}

function shoot( callback ) {
    if ( !isMac() ) {
        callback();
    }

    var showDf = spawn('osascript', ['-e','activate application "Dragonframe"']);
    showDf.on('exit', function ( code, signal ) {
        setTimeout(function () {
            // var enterInDF = spawn(path.join(process.cwd(), 'cliclick/cliclick'), ['w:500', 'm:+1,+1', 'm:-1,-1', 'kp:return']); // for some reason moving the mouse seems to increase effectiveness of the 'return' key using cliclick
            var enterInDF = spawn('osascript', ['-e','tell application "System Events"\ndelay 0.5\nkeystroke return\nend tell']);
            enterInDF.on('exit', function ( code, signal ) {
                callback();
            });
        }, 500);
    });
}

var CHARACTERISTIC_ID = '713d0000503e4c75ba943148f18d941e';
var $errorText;
var state = {
    position: 0,
    direction: true,
    locations: [],
    status: 'idle',
};

if ( isMac() ) {
    var menu = new gui.Menu({ type: 'menubar' });
    menu.createMacBuiltin && menu.createMacBuiltin(window.document.title);
    gui.Window.get().menu = menu;
}

function error( err ) {
    if ( err ) {
        console.error(arguments);
        console.error(err.stack ? err.message + ' - ' + err.stack : err);
        $errorText && $errorText.text && $errorText.text(err);
    }
}

function log() {
    console.log(arguments);
}

function ensureBuffer( buffer ) {
    if ( !Buffer.isBuffer(buffer) ) {
        buffer = Buffer(buffer);
    }
    return buffer;
}

function sliderWrite( buffer ) {
    buffer = ensureBuffer(buffer);
    log('Not sending because not connected: ' + buffer.toString('ascii'));
}

function setupNoble() {
    try {
        var noble = require('noble');

        noble.on('discover', function ( peripheral ) {
            var RSSI_THRESHOLD = -90;
            if ( peripheral.rssi < RSSI_THRESHOLD || peripheral.advertisement.localName != 'Slider' ) {
                return;
            }

            log(peripheral.rssi);
            log(peripheral.advertisement.localName);

            function connect() {
                peripheral.connect(function ( err ) {
                    if ( err ) {
                        return error(err);
                    }

                    peripheral.once('disconnect', function () {
                        log('Disconnected...');
                        connect();
                    });

                    peripheral.discoverAllServicesAndCharacteristics(function ( err, services, characteristics ) {
                        if ( err ) {
                            return error(err);
                        }

                        var service = noble._services[peripheral.id][CHARACTERISTIC_ID];

                        service.characteristics[1].on('read', function ( data, isNotification ) {
                            data = data.toString('ascii');
                            update(data);
                        });
                        service.characteristics[1].notify(true, error);

                        sliderWrite = function ( buffer ) {
                            buffer = ensureBuffer(buffer);
                            log('Sending: ' + buffer.toString('ascii'));
                            service.characteristics[0].write(buffer, true, error);
                        }

                        log('Ready!');
                        $errorText && $errorText.text && $errorText.text('');
                    });
                });
            }

            connect();
        });

        noble.on('stateChange', function ( state ) {
            log('State Change: ' + state);
            if (state === 'poweredOn') {
                noble.startScanning([], false);
            } else {
                // noble.stopScanning();
            }
        });
    } catch ( err ) {
        error(err);
        setTimeout(setupNoble, 5000);
    }
}
setupNoble();

function requestUpdate() {
    sliderWrite('?');
}

function update( data ) {
    data = data.replace(/\0/g, '') || '';
    var parts = data.split(/\s+/);
    for (var i = 0; i < parts.length; i++) {
        var part = parts[i];
        if ( part.indexOf('P:') == 0 ) {
            setPosition(part.substring(2));
        }
        if ( part.indexOf('D:') == 0 ) {
            setDirection(part.substring(2));
        }
        if ( part.indexOf('L:') == 0 ) {
            setLocations(part.substring(2));
        }
        if ( part.indexOf('S:') == 0 ) {
            setStatus(part.substring(2));
        }
        if ( part.indexOf('SHOOT') == 0 ) {
            shoot(function ( err ) {
                if ( err ) {
                    error(err);
                }

                setTimeout(function () {
                    sliderWrite('^');

                    if ( isMac() ) {
                        spawn('osascript', ['-e','activate application "nwjs"']);
                    }
                }, 1000);
            });
        }
    }

    function setPosition( p ) {
        p = parseInt(p);
        state.position = p;
        updateView();
    }

    function setDirection( d ) {
        d = parseInt(d);
        state.direction = (d=='0');
        updateView();
    }

    function setLocations( l ) {
        var lParts = l.split('-');
        if ( lParts.length != 2 ) {
            return;
        }

        state.locations[lParts[0]] = lParts[1];
        updateView();
    }

    function setStatus( s ) {
        log('status: ' + s);
        s = s.toLowerCase();
        if ( state.status != s ) {
            state.status = s;
            if ( s == 'idle' ) {
                if ( $('#bezier-auto-load')[0].checked ) {
                    var currentStep = parseFloat($('#bezier-current-step').val());
                    var totalPoints = parseFloat($('#bezier-steps').val());

                    $('#bezier-current-step').val(currentStep + 1).trigger('input');

                    // autoplay
                    if ( currentStep < totalPoints && $('#bezier-auto-play')[0].checked ) {
                        go();
                    }
                }
            }
            updateView();
        }
    }

    function updateView() {
        $('#position-text').text(state.position + 'mm');

        $('#direction-text').text(state.direction ? 'Home' : 'Away');

        for (var i = 0; i < state.locations.length; i++) {
            var location = state.locations[i];
            $('.location[data-id="' + i + '"] .value').text(location + 'mm');
        }
    }
}

function goAway() {
    sliderWrite('A');
    requestUpdate();
}

function reverse() {
    sliderWrite('B');
    requestUpdate();
}

function goHome() {
    sliderWrite('H');
    requestUpdate();
}

function goToLocation( l ) {
    sliderWrite(l + 'D');
    requestUpdate();
}

function saveLocation( l ) {
    sliderWrite(l + 'C');
    requestUpdate();
}

function calibrate() {
    var direction = $('#calibrate-direction')[0].checked ? 1 : 0;
    sliderWrite($('#calibrate-position').val() * 2 + direction + 'T');
    requestUpdate();
}

function go() {
    sliderWrite($('#movement-mm').val() + '#');
    sliderWrite($('#movement-shots').val() + '*');
    sliderWrite($('#movement-repeat').val() + 'M');
    requestUpdate();
}

function stop() {
    $('#bezier-auto-play')[0].checked = false; // TODO: should I do more of this here? Sometimes Stop doesn't quite work.
    sliderWrite('S');
    requestUpdate();
}

function loadBezier() {
    $('#movement-repeat').val(1);

    var totalPoints = parseFloat($('#bezier-steps').val());
    if ( totalPoints < 2 ) {
        $('#bezier-steps').val(2);
        totalPoints = parseFloat($('#bezier-steps').val());
    }

    var currentStep = parseFloat($('#bezier-current-step').val());
    if ( currentStep > totalPoints ) {
        // todo: I don't like this. also seems like bad MVC
        $('#bezier-current-step').val(totalPoints);
        currentStep = parseFloat($('#bezier-current-step').val());
    }

    if ( currentStep < 1 ) {
        $('#bezier-current-step').val(1);
        currentStep = parseFloat($('#bezier-current-step').val());
    }

    var x1 = parseFloat($('#bezier-params-x1').val());
    var y1 = parseFloat($('#bezier-params-y1').val());
    var x2 = parseFloat($('#bezier-params-x2').val());
    var y2 = parseFloat($('#bezier-params-y2').val());

    if ( x1 < 0 ) {
        $('#bezier-params-x1').val(0);
        x1 = 0;
    }
    if ( x2 < 0 ) {
        $('#bezier-params-x2').val(0);
        x2 = 0;
    }
    if ( x1 > 1 ) {
        $('#bezier-params-x1').val(1);
        x1 = 1;
    }
    if ( x2 > 1 ) {
        $('#bezier-params-x2').val(1);
        x2 = 1;
    }

    var bezier = {
        totalPoints,
        x1,
        y1,
        x2,
        y2,
    }

    var mm = 0;
    if ( currentStep < totalPoints ) {
        mm = Math.round(parseFloat($('#bezier-distance').val()) * (getBezierPoint(currentStep + 1, bezier).y - getBezierPoint(currentStep, bezier).y));
    }
    $('#movement-mm').val(mm);
}

function drawBezier() {
    var canvas = $('#bezier-canvas');
    var canvasElt = canvas[0];

    // todo: so much duplication here!
    var totalPoints = parseFloat($('#bezier-steps').val());
    if ( totalPoints < 2 ) {
        $('#bezier-steps').val(2);
        totalPoints = parseFloat($('#bezier-steps').val());
    }

    var currentStep = parseFloat($('#bezier-current-step').val());
    if ( currentStep > totalPoints ) {
        // todo: I don't like this. also seems like bad MVC
        $('#bezier-current-step').val(totalPoints);
        currentStep = parseFloat($('#bezier-current-step').val());
    }

    if ( currentStep < 1 ) {
        $('#bezier-current-step').val(1);
        currentStep = parseFloat($('#bezier-current-step').val());
    }

    var x1 = parseFloat($('#bezier-params-x1').val());
    var y1 = parseFloat($('#bezier-params-y1').val());
    var x2 = parseFloat($('#bezier-params-x2').val());
    var y2 = parseFloat($('#bezier-params-y2').val());

    if ( x1 < 0 ) {
        $('#bezier-params-x1').val(0);
        x1 = 0;
    }
    if ( x2 < 0 ) {
        $('#bezier-params-x2').val(0);
        x2 = 0;
    }
    if ( x1 > 1 ) {
        $('#bezier-params-x1').val(1);
        x1 = 1;
    }
    if ( x2 > 1 ) {
        $('#bezier-params-x2').val(1);
        x2 = 1;
    }

    var bezier = {
        totalPoints,
        x1,
        y1,
        x2,
        y2,
    }

    var h = canvas.height();
    canvasElt.width = h;
    canvasElt.height = h;
    var ctx = canvasElt.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(0, h);
    ctx.bezierCurveTo(h*x1, h*(1-y1), h*x2, h*(1-y2), h, 0);
    ctx.stroke();

    for (var i = 0; i < totalPoints; i++) {
        ctx.beginPath();
        ctx.fillStyle = (i == currentStep - 1) ? 'blue' : 'green';
        var radius = (i == currentStep - 1) ? 6 : 4;
        ctx.arc(getBezierPoint(i + 1, bezier).x * h, (1-getBezierPoint(i + 1, bezier).y) * h, radius, 0, 2 * Math.PI, true);
        ctx.fill();
    }

    $('#bezier-control-point-1').css({bottom: h * y1, left: h * x1})
    $('#bezier-control-point-2').css({bottom: h * y2, left: h * x2})
}

function getBezierPoint( index, bezier ) {
    var t = (index - 1)/(bezier.totalPoints - 1);

    return {
        x: 3 * Math.pow(1-t, 2) * t * bezier.x1 + 3 * Math.pow(t, 2) * (1-t) * bezier.x2 + Math.pow(t,3),
        y: 3 * Math.pow(1-t, 2) * t * bezier.y1 + 3 * Math.pow(t, 2) * (1-t) * bezier.y2 + Math.pow(t,3),
    }
}

setInterval(function () {
    requestUpdate();
}, 5000);

var $doc = $(document);
$doc.ready(function () {
    resize();
    loadBezier();
    drawBezier();

    $errorText = $('#error-text');

    var keyboardLocked = false;
    $doc.on('keydown', function ( e ) {
        if ( e.which == 32 ) { // space
            stop();
            return false;
        }

        if ( e.which == 76 && (e.metaKey || e.ctrlKey) ) { // ctrl + l
            keyboardLocked = !keyboardLocked;
            $('#keyboard-locked').toggle();
            resize();
        }

        if ( keyboardLocked ) {
            return false;
        } else if ( e.which == 13 ) { // Enter
            go();
        } else {
            if ( e.target != $('body')[0] ) {
                return;
            }

            if ( e.ctrlKey && e.which == 68 ) {
                gui.Window.get().showDevTools();
            } else if ( e.which == 39 ) { // right arrow
                reverse();
            } else if ( e.which == 38 ) { // up arrow
                reverse();
            } else if ( e.which == 40 ) { // down arrow
                reverse();
            } else if ( e.which == 37 ) { // left arrow
                reverse();
            } else if ( e.which == 72 ) { // 'h'
                goHome();
            } else if ( e.which == 65 ) { // 'a'
                goAway();
            } else {
                return;
            }
        }

        return false;
    });

    $('.go-location').click(function () {
        goToLocation($(this).closest('.location').data('id'));
        return false;
    });

    $('.save-location').click(function () {
        saveLocation($(this).closest('.location').data('id'));
        return false;
    });

    $('.load-bezier-preset').click(function () {
        var points = localStorage.getItem('bezier-bank-' + $(this).closest('.bezier-preset').data('id'));
        if ( points ) {
            points = JSON.parse(points);

            $('#bezier-params-x1').val(points.x1);
            $('#bezier-params-y1').val(points.y1);
            $('#bezier-params-x2').val(points.x2);
            $('#bezier-params-y2').val(points.y2).trigger('input'); // todo: this triggering seems like a bad way to do MVC
        }
        return false;
    });

    $('.save-bezier-preset').click(function () {
        var id = $(this).closest('.bezier-preset').data('id');
        if ( confirm('This will overwrite bank ' + id + '. Are you sure?') ) {
            var points = {
                x1: $('#bezier-params-x1').val(),
                y1: $('#bezier-params-y1').val(),
                x2: $('#bezier-params-x2').val(),
                y2: $('#bezier-params-y2').val(),
            }

            localStorage.setItem('bezier-bank-' + id, JSON.stringify(points));
        }
        return false;
    });

    $('#go-away').click(function () {
        goAway();
    });

    $('#go-home').click(function () {
        goHome();
    });

    $('#go').click(function () {
        go();
    });

    $('#stop').click(function () {
        stop();
    });

    $('#show-calibration, #calibration-cancel, #calibration-go').click(function () {
        $('#calibration').toggle();
    });

    $('#calibration-go').click(function () {
        calibrate();
    });

    $('#bezier-title').click(function () {
        $('.bezier-toggle').toggle();
        resize();
    });

    $('#open-cubic-bezier').click(function () {
        gui.Window.open('http://cubic-bezier.com/');
        return false;
    });

    $('#load-bezier').click(function () {
        loadBezier();
    });

    $('#bezier-steps, #bezier-params-x1, #bezier-params-y1, #bezier-params-x2, #bezier-params-y2, #bezier-current-step, #bezier-distance').on('input', function () {
        if ( state.status == 'idle' && $('#bezier-auto-load')[0].checked ) {
            loadBezier();
        }

        drawBezier();
    });

    $('#bezier-control-point-1').mousedown(function () {
        var xParam = $('#bezier-params-x1');
        var yParam = $('#bezier-params-y1');

        dragBezier(xParam, yParam);
    });

    $('#bezier-control-point-2').mousedown(function () {
        var xParam = $('#bezier-params-x2');
        var yParam = $('#bezier-params-y2');

        dragBezier(xParam, yParam);
    });

    function dragBezier( xParam, yParam ) {
        var canvas = $('#bezier-canvas');
        var h = canvas.height();
        var canvasX = canvas.offset().left;
        var canvasY = canvas.offset().top;

        $doc.mousemove(mousemove);
        $doc.mouseup(function () {
            $doc.off('mousemove', mousemove)
        });
        $doc.mouseleave(function () {
            $doc.off('mousemove', mousemove)
        });

        function mousemove( e ) {
            var x = Math.round(1000 * (e.pageX - canvasX) / h) / 1000;
            var y = Math.round(1000 * (h - (e.pageY - canvasY)) / h) / 1000;
            xParam.val(x);
            yParam.val(y).trigger('input'); // todo: this triggering seems like a bad way to do MVC
        }
    }
});

gui.Window.get().show();
