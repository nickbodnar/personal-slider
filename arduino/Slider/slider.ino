#include <SPI.h>
#include <EEPROM.h>
#include <boards.h>
#include <RBL_nRF8001.h>

#define DEBUG               false
#define ALTERNEWLINES       false
#define LOCATIONS_COUNT     10
#define STEPS_PER_MM        71l // 1mm
#define MAX_POSITION        STEPS_PER_MM * 1440l
#define UNINTERRUPTED_STEPS 10 * STEPS_PER_MM
#define SHOT_COMPLETE       4
#define STATUS_IDLE         "IDLE"
#define STATUS_MOVING       "MOVING"
#define STATUS_SHOOTING     "SHOOTING"

#define PIN_LED        13
#define PIN_DIR        A0 // yellow wire as of 11/29/2016
#define PIN_STEP       A1 // black wire as of 11/29/2016
#define PIN_BLE_REQN   7
#define PIN_BLE_RDYN   8

struct State
{
  long position;
  boolean step;
  boolean directionAway;
  boolean directionCalibration;
  int mmPerMovement;
  int shotsPerMovement;
  long locations[LOCATIONS_COUNT];
};

struct Pending
{
  long position;
  int movements;
  boolean moveAfterShot;
  int shotsToInitiate;
  int shotsUntilMove;

  long thisShot;
};

String current_status = STATUS_IDLE;
Pending pending;
State state;

long number;
boolean numberChanged = false;
boolean needsStatePut = false;
char key;
char lastKey;
int dfCmd;

void setup()
{
  pinMode(PIN_STEP, OUTPUT);
  pinMode(PIN_DIR, OUTPUT);

  EEPROM.get(0, state);

  //  reset();

  resetPending();

  writeDir();
  writeStep();

  setupBT();

#if DEBUG
  Serial.begin(57600);
#endif
}

void setupBT()
{
  ble_set_pins(PIN_BLE_REQN, PIN_BLE_RDYN);
  char s[] = "Slider";
  ble_set_name(s);
  ble_begin();
}

void reset()
{
  state.position = MAX_POSITION / 2;
  state.step = true;
  state.directionAway = true;
  state.directionCalibration = true;
  state.mmPerMovement = 0;
  state.shotsPerMovement = 1;
  for (int i = 0; i < LOCATIONS_COUNT; i++)
  {
    state.locations[i] = 0;
  }
  needsStatePut = true;
}

void loop()
{
  readBT();

  workTowardDesiredState();
}

void resetPending()
{
  pending.position = state.position;
  pending.movements = 0;
  pending.moveAfterShot = false;
  pending.shotsToInitiate = 0;
  pending.thisShot = 0;
  pending.shotsUntilMove = state.shotsPerMovement;
}

void workTowardDesiredState()
{
  if ( dfCmd != 0 )
  {
    debug("DF command: " + String(dfCmd));
  }

  // I'm done moving
  if ( pending.position == state.position )
  {
    // Just completed the shot this instant
    if ( dfCmd == SHOT_COMPLETE )
    {
      dfCmd = 0;
      pending.thisShot = 0;
      if ( pending.shotsUntilMove > 0 )
      {
        pending.shotsUntilMove = (pending.shotsUntilMove - 1);
      }
      if ( pending.shotsUntilMove == 0 )
      {
        if ( pending.moveAfterShot )
        {
          pending.position = addToPosition(state.mmPerMovement);
          pending.shotsUntilMove = state.shotsPerMovement;
        }
        pending.moveAfterShot = false;
      }
    }
    else
    {
      // not in the middle of a shot
      if ( pending.thisShot == 0 )
      {
        // tell camera to shoot
        if ( pending.shotsToInitiate > 0 )
        {
          send("SHOOT");
          setStatus(STATUS_SHOOTING);
          pending.thisShot = millis();
          pending.shotsToInitiate = (pending.shotsToInitiate - 1);
        }
        else
        {
          // no more shots to take. I already moved, so start over.
          if ( pending.movements > 0 )
          {
            pending.movements--;
            pending.moveAfterShot = true;
            pending.shotsToInitiate = state.shotsPerMovement;
            pending.shotsUntilMove = state.shotsPerMovement;
          }
          else
          {
            setStatus(STATUS_IDLE);
          }
        }
      }
      else
      {
        if ( millis() - pending.thisShot > 6000 )
        {
          pending.thisShot = 0;
          pending.movements = 0;
          pending.moveAfterShot = false;
          pending.shotsUntilMove = state.shotsPerMovement;
          // this is almost a reset but for:
          // pending.shotsToInitiate = 0;
        }
      }
    }

    if ( needsStatePut )
    {
      EEPROM.put(0, state);
      needsStatePut = false;
      debug("saved");
    }
  }
  else
  {
    setStatus(STATUS_MOVING);
    debug(String(pending.position) + ", " + String(state.position));

    long steps = pending.position - state.position;
    if ((steps > 0) != state.directionAway)
    {
      reverse();
    }
    takeManySteps(min(UNINTERRUPTED_STEPS, abs(steps)));
  }
}

void sendState()
{
  send("P:" + String(state.position / STEPS_PER_MM));
  send("D:" + String(state.directionAway));
  for (int i = 0; i < LOCATIONS_COUNT; i++)
  {
    send("L:" + String(i) + "-" + String(state.locations[i] / STEPS_PER_MM));
  }
  send("S:" + current_status);
}

void send(String s)
{
  //  debug("Sending: " + s);
  int l = s.length() + 1;
  byte c[l];
  s.getBytes(c, l);
  ble_write_bytes(c, l);
  ble_do_events();
}

void readBT()
{
  //  if ( !ble_connected() )
  //  {
  //    debug("Disconnected.");
  //  }

  boolean changed = true;
  while ( changed ) {
    changed = false;
    if ( ble_available() )
    {
      changed = true;
      key = ble_read();
      processKey();
    }

    ble_do_events();
  }
}

void processKey()
{
  lastKey = key;

  if (!numberChanged)
  {
    number = 0;
  }
  else
  {
    numberChanged = false;
  }

  debug("key: " + String(key));

  switch (key)
  {
    case '>':
      pending.position = addToPosition(number);
      break;
    case 'M':
      pending.movements = number;
      break;
    case 'B':
      reverse();
      break;
    case 'C':
      setLocation(number);
      break;
    case 'D':
      pending.position = getLocation(number);
      break;
    case 'H':
      pending.position = 0;
      break;
    case 'A':
      pending.position = MAX_POSITION;
      break;
    case '*':
      state.shotsPerMovement = number;
      needsStatePut = true;
      break;
    case '#':
      state.mmPerMovement = number;
      needsStatePut = true;
      break;
    case 'S':
      state.mmPerMovement = 0;
      state.shotsPerMovement = 0;
      resetPending();
      break;
    case 'T':
      // two values shoved into one. number is (position * 2 + boolean)
      state.position = (number / 2) * STEPS_PER_MM;
      if ( (number % 2) > 0 ) {
        state.directionCalibration = !(state.directionCalibration);
        writeDir();
      }
      state.mmPerMovement = 0;
      state.shotsPerMovement = 0;
      for (int i = 0; i < LOCATIONS_COUNT; i++)
      {
        state.locations[i] = 0;
      }
      resetPending();
      needsStatePut = true;
    case '?':
      sendState();
      break;
    case '^':
      dfCmd = SHOT_COMPLETE;
      break;
    default:
      number = 10 * number + (key - '0');
      numberChanged = true;
      debug(String(number));
      break;
  }
}

long addToPosition(long mm)
{
  long temp = (STEPS_PER_MM * mm * (state.directionAway ? 1 : -1));
  long newPosition = state.position + temp;
  if ( newPosition < 0 )
  {
    newPosition = 0;
  }
  if ( newPosition > MAX_POSITION )
  {
    newPosition = MAX_POSITION;
  }
  return newPosition;
}

void takeManySteps(long n)
{
  debug("Stepping: " + String(n));
  while (n-- > 0)
  {
    takeStep();
    delay(2);
  }
}

void takeStep()
{
  state.step = !(state.step);

  writeStep();

  if (state.directionAway)
  {
    state.position++;
  }
  else
  {
    state.position--;
  }

  needsStatePut = true;
}

void setStatus(String s)
{
  current_status = s;
  sendState();
}

void setLocation(int index)
{
  if (index < 0 || index > LOCATIONS_COUNT)
  {
    return;
  }
  state.locations[index] = state.position;
  needsStatePut = true;
}

long getLocation(int index)
{
  if (index < 0 || index > LOCATIONS_COUNT)
  {
    return state.position;
  }
  return state.locations[index];
}

void reverse()
{
  debug("reverse!");
  state.directionAway = !(state.directionAway);

  writeDir();
  needsStatePut = true;
}

void writeDir()
{
  if (state.directionAway ^ state.directionCalibration)
  {
    digitalWrite(PIN_DIR, HIGH);
  }
  else
  {
    digitalWrite(PIN_DIR, LOW);
  }
}

void writeStep()
{
  if (state.step)
  {
    digitalWrite(PIN_STEP, HIGH);
  }
  else
  {
    digitalWrite(PIN_STEP, LOW);
  }
}

inline void debug(String s)
{
#if DEBUG
  Serial.println("Debug: " + s);
#endif
}

void error(int delayLength)
{
  while (true)
  {
    blink(delayLength);
  }
}

void blink(int delayLength)
{
  digitalWrite(PIN_LED, HIGH);
  delay(delayLength);
  digitalWrite(PIN_LED, LOW);
  delay(delayLength);
}
