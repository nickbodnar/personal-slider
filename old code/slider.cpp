#include <wiringPi.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>

#define pin_0 0
#define pin_1 1
#define pin_2 2
#define pin_3 3

#define NO_KEY -1

struct termios orig_termios;

void cancel_getch() {
  tcsetattr(0, TCSANOW, &orig_termios);
}

void set_getch() {
  struct termios new_termios;

  tcgetattr(0, &orig_termios);
  memcpy(&new_termios, &orig_termios, sizeof(new_termios));

  atexit(cancel_getch);
  cfmakeraw(&new_termios);
  new_termios.c_cc[VTIME] = 0;
  new_termios.c_cc[VMIN] = 0;

  tcsetattr(0, TCSANOW, &new_termios);
}

int getch() {
  int r;
  unsigned char c;

  if ((r = read(0, &c, sizeof(c))) <= 0) {
    return NO_KEY;
  } else {
    return c;
  }
}

int main ()
{
  int c = 0;
  int done = 0;

  set_getch();

  while (!done) {
    c = getch();

    switch(c) {
      case 3:
        done = -1;
        break;
      case 'a':
        printf("a pressed\r\n");
        break;
      default:
        //printf("%i\r\n", c);
        break;
    }

    usleep(100);
  }
} 

int blah() {
  if(!wiringPiSetup()) {
    //return -1;
  } 

  pinMode (pin_0, OUTPUT);
  pinMode (pin_1, OUTPUT);
  pinMode (pin_2, OUTPUT);
  pinMode (pin_3, OUTPUT);

  long sleepTime = 0;
  long delay = 16;
  long thisStep = 0;
  while(true) {
    while(millis() - sleepTime < delay){}
    sleepTime = millis();
    long compare = thisStep % 4;
    thisStep++;

    switch (compare) {
      case 0:    // 1010
      digitalWrite(pin_0, HIGH);
      digitalWrite(pin_1, LOW);
      digitalWrite(pin_2, HIGH);
      digitalWrite(pin_3, LOW);
      break;
      case 1:    // 0110
      digitalWrite(pin_0, LOW);
      digitalWrite(pin_1, HIGH);
      digitalWrite(pin_2, HIGH);
      digitalWrite(pin_3, LOW);
      break;
      case 2:    //0101
      digitalWrite(pin_0, LOW);
      digitalWrite(pin_1, HIGH);
      digitalWrite(pin_2, LOW);
      digitalWrite(pin_3, HIGH);
      break;
      case 3:    //1001
      digitalWrite(pin_0, HIGH);
      digitalWrite(pin_1, LOW);
      digitalWrite(pin_2, LOW);
      digitalWrite(pin_3, HIGH);
      break;
    } 
  }

  digitalWrite(pin_0, LOW);
  digitalWrite(pin_1, LOW);
  digitalWrite(pin_2, LOW);
  digitalWrite(pin_3, LOW);

  return 0;
}